/*
 * Generate permutation
 * Or, use library function next_permutation(a.begin(),a.end()),
 * prev_permutation(a.begin(),a.end()),
 * is_permutation(a.begin(),a.end(),b.begin()).
 */
#include<bits/stdc++.h>
using namespace std;
#define fast_io ios_base::sync_with_stdio(false); cin.tie(nullptr)
#define rep(i, x, y) for(decltype(y) i = (x) - ((x) > (y)); i != (y) - ((x) > (y)); i += 1 - 2 * ((x) > (y)))
#define trav(i,a) for(auto& (i) : (a))
#define all(a) (a).begin(),(a).end()
#define sz(a) int((a).size())
#define clr(a,x) memset((a),(x),sizeof(a)) // array (0,-1)
#define dbg(x) cerr << "STDERR => " << __LINE__ << ": " << #x << " = " << (x) << "  :(\n";
inline void cpuTime() { cerr << "cpu time : " << double(clock())/CLOCKS_PER_SEC << "\n"; return; }
using ll = long long;
using ull = unsigned long long;
using F = float;
using D = double;
using vi = vector<int>;
using pii = pair<int,int>;

int n;
vi a,permute;
vector<bool>c;
void search(){
    
	if(permute.size() == n){
		for(auto& i : permute) cout << i << " ";
		cout << "\n";
		return;
	}
	rep(i,0,n){
		if(c[i]) continue;
		c[i]=true;
		permute.push_back(a[i]);
		search();
		c[i]=false;
		permute.pop_back();
	}
}

int main(){
    
    fast_io;
    n = 3;
    c.resize(n);
    a = {0,1,2};
    search();
	
    cpuTime();
return 0;
}
