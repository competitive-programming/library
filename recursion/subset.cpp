/*
 * generate all subsets from a set
 */
#include<bits/stdc++.h>
using namespace std;
#define fast_io ios_base::sync_with_stdio(false); cin.tie(nullptr)
#define rep(i, x, y) for(decltype(y) i = (x) - ((x) > (y)); i != (y) - ((x) > (y)); i += 1 - 2 * ((x) > (y)))
#define trav(i,a) for(auto& (i) : (a))
#define all(a) (a).begin(),(a).end()
#define sz(a) int((a).size())
#define clr(a,x) memset((a),(x),sizeof(a)) // array (0,-1)
#define dbg(x) cerr << "STDERR => " << __LINE__ << ": " << #x << " = " << (x) << "  :(\n";
inline void cpuTime() { cerr << "cpu time : " << double(clock())/CLOCKS_PER_SEC << "\n"; return; }
using ll = long long;
using ull = unsigned long long;
using F = float;
using D = double;
using vi = vector<int>;
using pii = pair<int,int>;

int n;
vi a,subset;
void search(int k){
    
	if(k == n){
		for(auto&i : subset)
			cout << i << " ";
		cout << "\n";
		return;
	}
	search(k+1);
	subset.push_back(a[k]);
	search(k+1);
	subset.pop_back();
}

int main(){
    
    fast_io;
    //solve();
    a = {0,1,2,3};
    n = int(a.size());
	search(0);
	
	for(ll b = 0;b < (1<<n);++b){
		vector<int>subset;
		rep(i,0,n){
			if(b&(1<<i)) subset.push_back(a[i]);
		}

		for(auto& i : subset) cout << i << " ";
		cout << "\n";
	}

    cpuTime();
return 0;
}
