// Maximum bipartite matching

/*
 This code performs maximum bipartite matching.
 Running time: O(|E| |V|) -- often much faster in practice
 INPUT: w[i][j] = edge between row node i and column node j
 OUTPUT: mr[i] = assignment for row node i, -1 if unassigned
 mc[j] = assignment for column node j, -1 if unassigned
 function returns number of matches made
 */
#include<vector>
using namespace std;
typedef vector<int> vi;
typedef vector<vi> vvi;


bool FindMatch(int i, const vvi &w, vi &mr, vi &mc, vi &seen) {
	for (int j = 0; j < w[i].size(); j++) {
		if (w[i][j] && !seen[j]) {
			seen[j] = true;
			if (mc[j] < 0 || FindMatch(mc[j], w, mr, mc, seen)) {
				mr[i] = j;
				mc[j] = i;
				return true;
			}
		}
	}
	return false;
}

int BipartiteMatching(const vvi &w, vi &mr, vi &mc) {
	mr = vi(w.size(), -1);
	mc = vi(w[0].size(), -1);
	int ct = 0;
	for (int i = 0; i < w.size(); i++) {
		vi seen(w[0].size());
		if (FindMatch(i, w, mr, mc, seen)) ct++;
	}
	return ct;
}
