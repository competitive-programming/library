/**
* Source: KACTL
* Description: Factors integers up to 2^{60}
* Usage: https://www.spoj.com/problems/FACT0/
*/
#include<bits/stdc++.h>
using namespace std;
#define rep(i, x, y) for(decltype(y) i = (x) - ((x) > (y)); i != (y) - ((x) > (y)); i += 1 - 2 * ((x) > (y)))
using ll = long long int;

const int bits = 3; // if all numbers are less than 2^k, set bits = 63-k
const ll po = 1<<bits;

ll mod_mul(ll a, ll b, ll &c) {
    // return (__int128(a)*b) % c;
    ll x = 0;
	for (; b; b >>= bits, a = (a<<bits)%c)
		x = (x + (a * (b & (po-1))) % c) % c;
	return x;
}

ll mod_pow(ll a, ll b, ll mod) {
	if (b == 0) return 1;
	ll res = mod_pow(a, b / 2, mod);
	res = mod_mul(res, res, mod);
	if (b & 1) return mod_mul(res, a, mod);
	return res;
}

bool prime(ll p) { // miller-rabin
	if (p == 2) return true;
	if (p == 1 || p % 2 == 0) return false;
	ll s = p - 1;
	while (s % 2 == 0) s /= 2;
	rep(i,0,15) {
		ll a = rand() % (p - 1) + 1, tmp = s;
		ll mod = mod_pow(a, tmp, p);
		while (tmp != p - 1 && mod != 1 && mod != p - 1) {
			mod = mod_mul(mod, mod, p);
			tmp *= 2;
		}
		if (mod != p - 1 && tmp % 2 == 0) return false;
	}
	return true;
}

ll f(ll a, ll n, ll &has) {
	return (mod_mul(a, a, n) + has) % n;
}
