/*
 *
 */
#include<bits/stdc++.h>
using namespace std;
#define fast_io ios_base::sync_with_stdio(false); cin.tie(nullptr)
#define rep(i, x, y) for(decltype(y) i = (x) - ((x) > (y)); i != (y) - ((x) > (y)); i += 1 - 2 * ((x) > (y)))
#define trav(i,a) for(auto& (i) : (a))
#define all(a) (a).begin(),(a).end()
#define sz(a) int((a).size())
#define clr(a,x) memset((a),(x),sizeof(a)) // array (0,-1)
#define dbg(x) cerr << "STDERR => " << __LINE__ << ": " << #x << " = " << (x) << "  :(\n";
inline void cpuTime() { cerr << "cpu time : " << double(clock())/CLOCKS_PER_SEC << "\n"; return; }
using ll = long long;
using ull = unsigned long long;
using F = float;
using D = double;
using vi = vector<int>;
using pii = pair<int,int>;

int getRandom(){
    uint32_t p = 1<<31;  // return 32bit random number
    seed_seq seq{
        (uint64_t) chrono::duration_cast<chrono::nanoseconds>(chrono::high_resolution_clock::now().time_since_epoch()).count(),
        (uint64_t) __builtin_ia32_rdtsc(),
        (uintptr_t) make_unique<char>().get()
    };
    mt19937 rng(seq);

    return uniform_int_distribution<int>(0, p-1)(rng);
}


inline int getRandomInt(int x, int y){
    return x + getRandom() % (y - x + 1);
}

// pseudo-random function
inline int f(int x, int c, int n) { // f(x) = x^2 + c (mod n)
    return (int)(1LL * x * x + c) % n;
}

int rho(const int n) { // returns a proper divisor of n or n (if it fails!)

    if (n % 2 == 0)
        return 2;

    int a = getRandomInt(1, n - 1);
    int c = getRandomInt(1, n - 1);
    int x = a;
    int y = a;
    int divisor;
    do{
        x = f(x, c, n);
        y = f(y, c, n);
        y = f(y, c, n);
        divisor = __gcd(abs(x - y), n);
    } while (divisor == 1);

    return divisor;
}

int pollard_rho(const int n) { // returns a proper divisor of n (IT FAILS IF n IS PRIME!)

    srand(time(nullptr));
    int d;
    do{
        d = rho(n);
    } while (d == n);
    return d;
}

int main(){
    
    fast_io;
    //solve();
    
    cout << pollard_rho(75) << "\n";

    cpuTime();
return 0;
}
