/*
 * Find all prime factors of a number
 */

#include<bits/stdc++.h>

int factor(int n){
	int a;
	if (n%2==0)
		return 2;
	for (a=3;a*a<=n;a++){
		if (n%a==0)
			return a;
	}
	return n;
}


int main(){

	// complete factorization
	int r,n;
	while (n>1) {
		r = factor(n);
		printf("%d", r);
		n /= r;
	}
}
