/*
 * Primality Test (more precisely compositeness test)
 * fails on number p*q (RSA)
 * where, p & q are very large prime number
 */
#include<bits/stdc++.h>
using namespace std;
#define fast_io ios_base::sync_with_stdio(false); cin.tie(nullptr)
#define rep(i, x, y) for(decltype(y) i = (x) - ((x) > (y)); i != (y) - ((x) > (y)); i += 1 - 2 * ((x) > (y)))
#define trav(i,a) for(auto& (i) : (a))
#define all(a) (a).begin(),(a).end()
#define sz(a) int((a).size())
#define clr(a,x) memset((a),(x),sizeof(a)) // array (0,-1)
#define dbg(x) cerr << "STDERR => " << __LINE__ << ": " << #x << " = " << (x) << "  :(\n";
inline void cpuTime() { cerr << "cpu time : " << double(clock())/CLOCKS_PER_SEC << "\n"; return; }
using ll = long long;
using ull = unsigned long long;
using F = float;
using D = double;
using vi = vector<int>;
using pii = pair<int,int>;

/*
 * Deterministic algo
 * NOTE : For better time complexity( O(lg(n^3)) ) use Randomized algo
 */
template<typename T>
T modular_exponentiation(T a, T p, T mod) { // a^p % mod
    T res = 1;
    while (p > 0){
        if (p & 1){
        	res = (T)(1LL*res*a) % mod;
        }
        else{
        	a = (T)(1LL*a*a) % mod;
        	p >>= 1;
        }
    }
    return res;
}

bool witness(int a, int n) { // check if n is composite using 'a' as 'witness'
    int x[64];
    int tmp = n - 1;
    int u, t = 0;

    while (tmp % 2 == 0){
        ++t; tmp >>= 1;
    }
    u = tmp;
    // n = u*(2^t)
    x[0] = modular_exponentiation(a, u, n);
    rep(i,1,t+1){
        x[i] = (int)(1LL * x[i-1] * x[i-1]) % n;
        if (x[i] == 1 && (x[i-1] != 1 && x[i-1] != n - 1))
            return true; // n is composite
    }

    if (x[t] != 1)
        return true; // n is composite
    else
        return false; // n is 'probably' prime
}
/*
 * if n == prime return true else false;
 */
bool miller_rabin(int n){
    if (n == 2 || n == 7 || n == 61)
        return true;
    if (n == 1 || n%2 == 0)
        return false;
    if (witness(2, n) || witness(7, n) || witness(61, n))
        return false; // n is composite

    return true; // n is 'probably' prime
}


int main(){
    
    fast_io;
    int a;
    cin >> a;
    cout << miller_rabin(a) << "\n";
    //solve();

	
    cpuTime();
return 0;
}
