/*
  Reduced row echelon form via Gauss-Jordan elimination with partial pivoting.
  This can be used for computing the rank of a matrix.
  Time complexity : O(nˆ3)
  in: a[][] = an nxm matrix
  out: rref[][] = an nxm matrix (stored in a[][])
  returns rank of a[][]
 */
#include<bits/stdc++.h>
using namespace std;
#define fast_io ios_base::sync_with_stdio(false); cin.tie(nullptr)
#define rep(i, x, y) for(decltype(y) i = (x) - ((x) > (y)); i != (y) - ((x) > (y)); i += 1 - 2 * ((x) > (y)))
#define dbg(x) cerr << "STDERR => " << __LINE__ << ": " << #x << " = " << (x) << "  :(\n";
inline void cpuTime() { cerr << "cpu time : " << double(clock())/CLOCKS_PER_SEC << "\n"; return; }
const double EPS = 1e-10;
using D = double;
using VD = vector<D>;
using VVD = vector<VD>;

int rref(VVD &a) {
	int n = int(a.size());
	int m = int(a[0].size());
	int r = 0;
	for (int c = 0; c < m && r < n; c++) {
		int j = r;
		rep(i,r+1,n)
		if (fabs(a[i][c]) > fabs(a[j][c])) j = i;
		if (fabs(a[j][c]) < EPS) continue;
		swap(a[j], a[r]);
		D s = 1.0 / a[r][c];
		rep(j,0,m) a[r][j] *= s;
		rep(i,0,n) if (i != r) {
			D t = a[i][c];
			rep(j,0,m) a[i][j] -= t * a[r][j];
		}
		r++;
	}
	return r;
}

int main() {
	const int n = 5, m = 4;
	double A[n][m] = { {16, 2, 3, 13},
					   { 5, 11, 10, 8},
					   { 9, 7, 6, 12},
					   { 4, 14, 15, 1},
					   {13, 21, 21, 13}
					 };
	VVD a(n);
	for(int i = 0;i < n;++i)
		a[i] = VD(A[i], A[i] + m);
	int rank = rref(a);
	// expected: 3
	cout << "Rank: " << rank << "\n";
/*	 expected:
     1 0 0 1
	 0 1 0 3
	 0 0 1 -3
	 0 0 0 3.10862e-15
	 0 0 0 2.22045e-15 */

	cout << "rref: " << "\n";
	rep(i,0,5) {
		rep(j,0,4)
			cout << a[i][j] << ' ';
		cout << "\n";
	}

	cpuTime();
	return 0;
}
