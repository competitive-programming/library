// sieve of erotosthenes

#include<bits/stdc++.h>
using namespace std;

#define MAX 1000000
typedef vector<int> VI;

VI prime;
vector<bool> is_composite(MAX,false);

void sieve(){
	for(int i = 2;i <= MAX;++i){
		if(!is_composite[i]){
			prime.push_back(i);
			for(int j = i*i;j <= MAX;j += i)
				is_composite[j] = true;
		}
	}

}


int main(){

	sieve();
	for(auto i : prime){
		cout << i << " ";
	}

	return 0;
}
