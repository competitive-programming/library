/*
 Compute the 2D convex hull of a set of points using the monotone chain
 algorithm.  Eliminate redundant points from the hull if REMOVE_REDUNDAND is  #defined.

 Running time: O(n log n)

   in:   a vector of input points, unordered.
   out:  a vector of points in the convex hull, counterclockwise, starting with bottommost/leftmost point
*/

#include<bits/stdc++.h>
using namespace std;
#define fast_io ios_base::sync_with_stdio(false); cin.tie(nullptr)
#define rep(i, x, y) for(decltype(y) i = (x) - ((x) > (y)); i != (y) - ((x) > (y)); i += 1 - 2 * ((x) > (y)))
#define trav(i,a) for(auto& (i) : (a))
#define all(a) (a).begin(),(a).end()
#define sz(a) int((a).size())
#define clr(a,x) memset((a),(x),sizeof(a)) // array (0,-1)
#define dbg(x) cerr << "STDERR => " << __LINE__ << ": " << #x << " = " << (x) << "  :(\n";
inline void cpuTime() { cerr << "cpu time : " << double(clock())/CLOCKS_PER_SEC << "\n"; return; }
using ll = long long;
using ull = unsigned long long;
using F = float;
using D = double;
using vi = vector<int>;
using pii = pair<int,int>;
#define REMOVE_REDUNDANT

const D EPS = 1e-7;
struct PD {
  D x, y;
  PD() {}
  PD(D x, D y) : x(x), y(y) {}
  bool operator<(const PD &rhs) const { return make_pair(y,x) < make_pair(rhs.y,rhs.x); }
  bool operator==(const PD &rhs) const { return make_pair(y,x) == make_pair(rhs.y,rhs.x); }
};

D cross(PD p, PD q) { return p.x*q.y-p.y*q.x; }
D area2(PD a, PD b, PD c) { return cross(a,b) + cross(b,c) + cross(c,a); }

#ifdef REMOVE_REDUNDANT
bool between(const PD &a, const PD &b, const PD &c) {
  return (fabs(area2(a,b,c)) < EPS && (a.x-b.x)*(c.x-b.x) <= 0 && (a.y-b.y)*(c.y-b.y) <= 0);
}
#endif

void ConvexHull(vector<PD> &pts) {
  sort(pts.begin(), pts.end());
  pts.erase(unique(pts.begin(), pts.end()), pts.end());
  vector<PD> up, dn;
  for (int i = 0; i < pts.size(); i++) {
    while (up.size() > 1 && area2(up[up.size()-2], up.back(), pts[i]) >= 0) up.pop_back();
    while (dn.size() > 1 && area2(dn[dn.size()-2], dn.back(), pts[i]) <= 0) dn.pop_back();
    up.push_back(pts[i]);
    dn.push_back(pts[i]);
  }
  pts = dn;
  for (int i = (int) up.size() - 2; i >= 1; i--) pts.push_back(up[i]);

#ifdef REMOVE_REDUNDANT
  if (pts.size() <= 2) return;
  dn.clear();
  dn.push_back(pts[0]);
  dn.push_back(pts[1]);
  for (int i = 2; i < pts.size(); i++) {
    if (between(dn[dn.size()-2], dn[dn.size()-1], pts[i])) dn.pop_back();
    dn.push_back(pts[i]);
  }
  if (dn.size() >= 3 && between(dn.back(), dn[0], dn[1])) {
    dn[0] = dn.back();
    dn.pop_back();
  }
  pts = dn;
#endif
}

// BEGIN CUT
// Dhe following code solves SPOJ problem #26: Build the Fence (BSHEEP)
int main(){
    
    fast_io;
    int t;
    scanf("%d", &t);
    for (int caseno = 0; caseno < t; caseno++) {
      int n;
      scanf("%d", &n);
      vector<PD> v(n);
      for (int i = 0; i < n; i++) scanf("%lf%lf", &v[i].x, &v[i].y);
      vector<PD> h(v);
      map<PD,int> index;
      for (int i = n-1; i >= 0; i--) index[v[i]] = i+1;
      ConvexHull(h);

      double len = 0;
      for (int i = 0; i < h.size(); i++) {
        double dx = h[i].x - h[(i+1)%h.size()].x;
        double dy = h[i].y - h[(i+1)%h.size()].y;
        len += sqrt(dx*dx+dy*dy);
      }

      if (caseno > 0) printf("\n");
      printf("%.2f\n", len);
      for (int i = 0; i < h.size(); i++) {
        if (i > 0) printf(" ");
        printf("%d", index[h[i]]);
      }
      printf("\n");
    }
    
    cpuTime();
return 0;
}
// END CUT
