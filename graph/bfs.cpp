#include<bits/stdc++.h>
using namespace std;
#define fast_io ios_base::sync_with_stdio(0); cin.tie(NULL)
#define rep(i, x, y) for(decltype(y) i = (x) - ((x) > (y)); i != (y) - ((x) > (y)); i += 1 - 2 * ((x) > (y)))
#define trav(i,a) for(auto& (i) : (a))
#define all(a) (a).begin(),(a).end()
#define sz(a) int((a).size())
using ll = long long;
using ull = unsigned long long;
using F = float;
using D = double;
using vi = vector<int>;
using pii = pair<int,int>;

/*
 * BFS : time complexity O(V+E)
 *
 */
#define MAX int(1e5)
#define inf int(1e9+7)
vector<vi> g(MAX);
vector<bool> seen(MAX,false);
vi d(MAX,inf);
void bfs(int u){

	fill(seen.begin(),seen.end(),false);
	d[u] = 0;
    seen[u] = true;
    queue<int> q;
    q.emplace(u);
    
    while(!q.empty()){
    	u = q.front();
    	q.pop();
    	for(auto& v : g){
    		seen[v] = true;
    		q.emplace(v);
    		d[v] = d[u]+1;
    	}
    }
}

int main(){

    fast_io;
    //bfs();


return 0;
}
