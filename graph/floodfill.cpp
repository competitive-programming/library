#include<bits/stdc++.h>
using namespace std;
#define fast_io ios_base::sync_with_stdio(false); cin.tie(nullptr)
#define rep(i, x, y) for(decltype(y) i = (x) - ((x) > (y)); i != (y) - ((x) > (y)); i += 1 - 2 * ((x) > (y)))
#define clr(a,x) memset((a),(x),sizeof(a)) // array (0,-1)
using vi = vector<int>;
using pii = pair<int,int>;
inline double cpuTime() { return double(clock())/CLOCKS_PER_SEC; }
const int MAX = 1000;

bool seen[MAX][MAX];
char matrix[MAX][MAX];
int r,c;
const vector<pii> moves = { pii{-1,0}, pii{1,0}, pii{0,-1}, pii{0,1} };

template<typename T>
void flood(int i, int j, T color){
    if(i < 0 || i >= r || j < 0 || j >= c || matrix[i][j] != color || seen[i][j])
      return;

    seen[i][j] = true;
    for(auto &move : moves) flood(i+move.first, j+move.second, color);
}

int cnt(){

  int ans = 0;
  rep(i,0,r){
    rep(j,0,c){
      if(!seen[i][j]){
        ++ans;
        flood(i,j,matrix[i][j]);
      }
    }
  }
  return ans;
}

int main(){
    
    fast_io;
    cin >> r >> c;
    rep(i,0,r){
      rep(j,0,c){
        cin >> matrix[i][j];
      }
    }

    fill((bool *)seen, (bool *)seen+r*c, false);
    cout << cnt() << "\n";

return 0;
}
