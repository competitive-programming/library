/*
 * Topological sorting : exist if graph is directed acyclic graph otherwise use dfs to find whether graph is acyclic??
 * or, there is at least one vertex must having in-degree 0
 * Time complexity : 1). O(v+e) => Robert Endre Trajan alogrithm
 * 					 2). O(v+e) => Khan's algorithm
 *
 */
#include<bits/stdc++.h>
using namespace std;
#define fast_io ios_base::sync_with_stdio(false); cin.tie(nullptr)
#define rep(i, x, y) for(decltype(y) i = (x) - ((x) > (y)); i != (y) - ((x) > (y)); i += 1 - 2 * ((x) > (y)))
#define pb push_back
using vi = vector<int>;
using pii = pair<int,int>;

int n;
vector<vi> g;
vector<bool> visited;
vi ans;

void dfs(int v) {
    visited[v] = true;
    for (auto& u : g[v]) {
        if (!visited[u])
            dfs(u);
    }
    ans.pb(v);
}

// Robert Endre Tarjan algorithm
void tsort() { // assuming graph is acyclic
    visited.assign(n, false);
    ans.clear();
    for (int i = 0; i < n; ++i) {
        if (!visited[i])
            dfs(i);
    }
    reverse(ans.begin(), ans.end());
}


//Khan's algorithm
void topsort(){

	vi id(n,0);
	priority_queue<pii, vector<pii>, greater<pii>> q;
	for(auto& i: g){
		for(auto& j : i){
			id[j]++;
		}
	}
	rep(i, 0, n){
		q.push({id[i],i});
	}
	while(!q.empty()){
		if(q.top().first() != 0) break;
		for(auto& i : g[q.top().second]){

		}
	}
}

int main(){
    
    fast_io;
    //solve();
    
    
return 0;
}
