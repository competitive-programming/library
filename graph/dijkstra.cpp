/*
 * Dijkstra : Single source shortest path using bfs algo
 * Time complexity : O()
 */
#include<bits/stdc++.h>
using namespace std;
#define fast_io ios_base::sync_with_stdio(false); cin.tie(nullptr)
#define rep(i, x, y) for(decltype(y) i = (x) - ((x) > (y)); i != (y) - ((x) > (y)); i += 1 - 2 * ((x) > (y)))
#define trav(i,a) for(auto& (i) : (a))
#define all(a) (a).begin(),(a).end()
#define sz(a) int((a).size())
#define clr(a,x) memset((a),(x),sizeof(a)) // array (0,-1)
const int INF = int (1e9+7);
using ll = long long;
using ull = unsigned long long;
using F = float;
using D = double;
using vi = vector<int>;
using pii = pair<int,int>;
const int MAX = 1000;


bool mark[MAX];
int d[MAX],n;
vector<vector<pii> >g; // g[i][j] = pair(Vertex,Weight)
void dijkstra(int v){
	g.resize(n);
	fill(d,d + n, INF);
	fill(mark, mark + n, false);
	d[v] = 0;
	int u;
	priority_queue<pii, vector<pii>, less<pii> > pq;
	pq.push({d[v], v});
	while(!pq.empty()){
		u = pq.top().second;
		pq.pop();
		if(mark[u])	continue;
		mark[u] = true;
		for(auto p : g[u]) //g[v][i] = pair(vertex, weight)
			if(d[p.first] > d[u] + p.second){
				d[p.first] = d[u] + p.second;
				pq.push({d[p.first], p.first});
			}
	}
}

int main(){
    
    fast_io;
    //solve();
    
    
return 0;
}
