/*
 * Connected component :
 * 1) cc (O(v*(v+e)))
 * 2) flood fill (O(v*v)) also called as seed fill (see ./fill.cpp)
 *
 */
#include<bits/stdc++.h>
using namespace std;
#define fast_io ios_base::sync_with_stdio(false); cin.tie(nullptr)
#define rep(i, x, y) for(decltype(y) i = (x) - ((x) > (y)); i != (y) - ((x) > (y)); i += 1 - 2 * ((x) > (y)))
using vi = vector<int>;
const int MAX = 1000;

vector<bool>isVisited(MAX,false);
vector<int>g[MAX];
int cnt = 0,n;

void bfs(int u){

	queue<int>q;
	q.push(u);
	int x;
	while(!q.empty()){
		x = q.front();
		q.pop();
		isVisited[x] = true;
		for(int j = 0;j < n;++j){
			if(g[x][j] == 1 && !isVisited[j]){
				isVisited[j] = true;
				q.push(j);
			}
		}
	}
	++cnt;
}

int main(){

	// TODO : using 0 index in graph

	cout << "# of vertices in graph G : ";
	cin >> n;


	for(int i = 0;i < n;i++){
		for(int j = 0;j < n;j++){
			cin >> g[i][j];
		}
	}

	for(int i = 0;i < n;++i){
		if(!isVisited[i]){
			bfs(i);
		}
	}
	cout << "# of connected components in graph g : " << cnt << "\n";

	for(int i = 0;i < n;i++){
		for(int j = 0;j < n;j++){
			g[i][j] ^= 1;
		}
	}

	cnt = 0;
	isVisited.assign(n,false);
	for(int i = 0;i < n;++i){
		if(!isVisited[i]){
			bfs(i);
		}
	}
	cout << "# of connected components in complement of graph g : " << cnt << "\n";

	return 0;
}

/*#include<bits/stdc++.h>
using namespace std;
int main(){
    
    fast_io;
    //solve();
    
    
return 0;
}*/


