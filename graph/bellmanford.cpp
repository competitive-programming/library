/* This function runs the Bellman-Ford algorithm for single source
 shortest paths with negative edge weights.  The function returns
 false if a negative weight cycle is detected.  Otherwise, the
 function returns true and dist[i] is the length of the shortest
 path from source to i.

 time complexity: O(n^3)

   in :   start, w[i][j] = cost of edge from i to j
   out :  dist[i] = min weight path from start to i
            prev[i] = previous node on the best path from the start node
*/
#include<bits/stdc++.h>
using namespace std;
#define fast_io ios_base::sync_with_stdio(false); cin.tie(nullptr)
#define rep(i, x, y) for(decltype(y) i = (x) - ((x) > (y)); i != (y) - ((x) > (y)); i += 1 - 2 * ((x) > (y)))
#define trav(i,a) for(auto& (i) : (a))
#define all(a) (a).begin(),(a).end()
#define sz(a) int((a).size())
#define clr(a,x) memset((a),(x),sizeof(a)) // array (0,-1)
#define dbg(x) cerr << "STDERR => " << __LINE__ << ": " << #x << " = " << (x) << "  :(\n";
inline void cpuTime() { cerr << "cpu time : " << double(clock())/CLOCKS_PER_SEC << "\n"; return; }
using ll = long long;
using ull = unsigned long long;
using F = float;
using D = double;
using vi = vector<int>;
using pii = pair<int,int>;

using VD = vector<D>;
using VVD = vector<VD>;
using vvi = vector<vi>;

bool BellmanFord (const VVD &w, VD &dist, VD &prev, int start){
  int n = w.size();
  prev = VD(n, -1);
  dist = VD(n, 1000000000);
  dist[start] = 0;

  rep(k,0,n){
    rep(i,0,n){
      rep(j,0,n){
        if (dist[j] > dist[i] + w[i][j]){
          if (k == n-1) return false;
          dist[j] = dist[i] + w[i][j];
          prev[j] = i;
        }
      }
    }
  }

  return true;
}

void solve(){
    
}

int main(){
    
    fast_io;
    //solve();
    
	
    cpuTime();
return 0;
}
