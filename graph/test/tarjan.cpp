#include<bits/stdc++.h>
using namespace std;
#define fast_io ios_base::sync_with_stdio(false); cin.tie(nullptr)
#define rep(i, x, y) for(decltype(y) i = (x) - ((x) > (y)); i != (y) - ((x) > (y)); i += 1 - 2 * ((x) > (y)))
#define trav(i,a) for(auto& (i) : (a))
#define all(a) (a).begin(),(a).end()
#define sz(a) int((a).size())
#define clr(a,x) memset((a),(x),sizeof(a)) // array (0,-1)
#define pb push_back
#define dbg(x) cerr << "STDERR => " << __LINE__ << ": " << #x << " = " << (x) << "  :(\n";
inline void cpuTime() { cerr << "cpu time : " << double(clock())/CLOCKS_PER_SEC << "\n"; return; }
using ll = long long;
using ull = unsigned long long;
using F = float;
using D = double;
using vi = vector<int>;
using pii = pair<int,int>;

/**
* Source: See BCC template
* Description: generates SCC in reverse topological order
*/

template<int SZ> struct scc {
    int N, ti = 0;
    vi adj[SZ], st, fin;
    int disc[SZ], low[SZ], comp[SZ];
    bitset<SZ> inStack;

    void addEdge(int u, int v) {
        adj[u].pb(v);
    }

    void SCCutil(int u) {
        disc[u] = low[u] = ti++;
        st.pb(u); inStack[u] = 1;

        for (int i: adj[u]) {
            if (disc[i] == -1) {
                SCCutil(i);
                low[u] = min(low[u],low[i]);
            } else if (inStack[i]) {
                low[u] = min(low[u],disc[i]);
            }
        }

        if (disc[u] == low[u]) {
            while (st.back() != u) {
                comp[st.back()] = u;
                inStack[st.back()] = 0;
                st.pop_back();
            }
            comp[u] = u; inStack[u] = 0; st.pop_back();
            fin.pb(u);
        }
    }
    
    void genSCC() {
        rep(i,1,N+1) disc[i] = low[i] = -1;
        rep(i,1,N+1) if (disc[i] == -1) SCCutil(i);
    }
};

int main(){
    
    fast_io;
    //solve();
    
	
    cpuTime();
return 0;
}
