/*
 * N queen problem
 */
#include<bits/stdc++.h>
using namespace std;
#define fast_io ios_base::sync_with_stdio(false); cin.tie(nullptr)
#define rep(i, x, y) for(decltype(y) i = (x) - ((x) > (y)); i != (y) - ((x) > (y)); i += 1 - 2 * ((x) > (y)))
#define trav(i,a) for(auto& (i) : (a))
#define all(a) (a).begin(),(a).end()
#define sz(a) int((a).size())
#define clr(a,x) memset((a),(x),sizeof(a)) // array (0,-1)
#define dbg(x) cerr << "STDERR => " << __LINE__ << ": " << #x << " = " << (x) << "  :(\n";
inline void cpuTime() { cerr << "cpu time : " << double(clock())/CLOCKS_PER_SEC << "\n"; return; }
using ll = long long;
using ull = unsigned long long;
using F = float;
using D = double;
using vi = vector<int>;
using pii = pair<int,int>;

int n; // no. of queens
int cnt; // count no. of ways to array n queens
vector<bool>col,dig1,dig2;
vector< vector<bool> >board;

inline void print_board(){
	cout << "count : " << cnt << "\n";
	rep(i,0,n){
		rep(j,0,n){
			if(board[i][j]) cout << "1 ";
			else cout << "0 ";
		}
		cout << "\n";
	}
}

void search(int y){

	if(y == n){
		++cnt;
		print_board();
		return;
	}
	rep(x,0,n){
		if(col[x] || dig1[x+y] || dig2[x-y+n-1]) continue;
		col[x] = dig1[x+y] = dig2[x-y+n-1] = true;
		board[y][x] = true;
		search(y+1);
		col[x] = dig1[x+y] = dig2[x-y+n-1] = false;
		board[y][x] = false;
	}

}

int main(){
    
    fast_io;
    cin >> n;
    col.resize(n),dig1.resize(2*n-1),dig2.resize(2*n-1);
    board = vector< vector<bool> >(n,vector<bool>(n,false));
    fill(all(col),false),fill(all(dig1),false),fill(all(dig2),false);
    search(0);
//	cout << cnt << "\n";

    cpuTime();
return 0;
}
