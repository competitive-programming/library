/*
 * Interpolation search : sorted data
 *
 * Time Complexity : O(log(log(n))) { uniformly distributed }
 * 				   : O(n) { exponentially distributed }
 */

#include<bits/stdc++.h>
using namespace std;

template <typename T>
int interpolation_search(vector<T> a, int n, T key){
    int low = 0;
    int high = n - 1;
    int mid;

    while ((a[high] != a[low]) && (key >= a[low]) && (key <= a[high])) {
        mid = low + ((key - a[low]) * (high - low) / (a[high] - a[low]));

        if (a[mid] < key)
            low = mid + 1;
        else if (key < a[mid])
            high = mid - 1;
        else
            return mid;
    }

    if (key == a[low])
        return low ;
    else
        return -1;
}

int main(){

	vector<int>a = {0,7,12,17,21,26,31,33,40,43,49,51,64,66,72,73,81,87,95,99};

	cout << interpolation_search(a,20,64) << "\n";;

	return 0;
}
